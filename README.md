# SignalPath
## Computer Systems Validation

### Introduction

#### The Law and Such
In [21 CFR Part 11](http://www.ecfr.gov/cgi-bin/text-idx?SID=614ebaf4ab0e38515b26d79f5ee10c33&mc=true&tpl=/ecfrbrowse/Title21/21cfr11_main_02.tpl), Federal Regulations require SignalPath to establish processes and controls that help us to ensure that we build high-quality software. SignalPath has created this collection of documents to outline those processes and controls. (Note that Federal Regulations **don't** require us to follow the FDA's standard processes and controls ... mainly because the FDA doesn't have them.)

Having a strong Computer Systems Validation process helps 3rd parties (such as hospital systems) to trust our software. By reviewing our controls and our adherance to them, they build confidence that our software is reliable.

#### Our Approach
At SignalPath, we are focused on delivering high-quality software at a rapid pace. Adhering to the regulations in 21 CFR Part 11 come quite naturally ... after all, they're there to help us do what we have done from the start. Of course, we also want to ensure that our Computer Systems Validation processes do not get in the way of us delivering software at a rapid pace.

Our approach, therefore, is to rely on the tools and technologies that are currently integrated into our workflow. Specifically, we will primarily utilize Jira, Bitbucket and Jenkins, relying on their workflows, permission structures and logging, to record sign-offs, track requirements, etc. Each phase describes in more detail the specifics.

#### Applicability to User-Facing Changes
These processes only apply to those changes that directly affect the functionality we provide our users.

**Examples: Directly Affect Users**

- Changing password requirements for the application
- Fixing a bug related to scheduling patients
- Improving the workflow for managing Terms of Service

**Examples: Don't Directly Affect Users**

- Introducing new unit tests
- Performing fixes to remove Exceptions in our log files
- Moving UI application from web servers to CloudFront

### Processes

#### New Features and Enhancements
1. [Business Requirements](sops/business_requirements.md)
1. [Stories](sops/stories.md)
1. [Architecture](sops/architecture.md)
1. [Implementation](sops/implementation.md)
1. [Deployment](sops/deployment.md)

#### Bug Fixes
1. [Bug Submittal](sops/bugs.md)
1. [Implementation](sops/implementation.md)
1. [Deployment](sops/deployment.md)

### Meta
Revisions to SignalPath's Computer Systems Validations can be suggested by those workforce members who already have sign-off responsibilities in one or more of the phases. Those suggestions do not become finalized until approved by the Chief Technology Officer. Upon approval by the CTO, the suggested changes are officially adopted when they are merged into "master" and given a new version number.

#### Versioning
This follows the `X.Y.Z` versioning scheme.

- `X` gets incremented whenever a phase is created or removed. `Y` and `Z` get set back to `0` when `X` is incremented.
- `Y` gets incremented whenever a meaningful change is made within a phase. `Z` gets set back to `0` when `Y` is incremented.
- `Z` gets incremented whenever a non-meaningful change (e.g. fixing a typo) is made within a phase or in the front-matter.

### License
Copyright 2016-2017, SignalPath LLC. All rights reserved.