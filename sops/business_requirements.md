# SignalPath Computer Systems Validation

## Business Requirements

### Overview
The purpose of the **Business Requirements** phase is to ensure that the Product Owner and Architectural Team understand the purpose of the functionality SignalPath plans to build.

### Entry Criteria
#### Required Artifacts
None

#### Optional Artifacts
None

### Exit Criteria

#### Required Artifacts
In order to leave the **Business Requirements** phase and move forward in our development process, we require that:

- A Jira issue of type "Major Feature" is created.
- The Jira issue has an appropriate summary.
- The Jira issue has text that describes, at a high level, the purpose of the feature.
- At least one of the following are created and attached to the Jira issue.
    - Use Case Diagrams
    - Wireframes
    - High-fidelity UI Mockups

#### Optional Artifacts

From time to time, it may be appropriate to create one or more of the following, and attach them to the Jira issue.

- Design Documents
- Activity Diagrams

#### Sign Off
A member of the Product Management team signs off on the **Business Requirements** for a major feature by advancing the issue to the status of "Ready for Stories". The signee must specify one of the following when signing off:

- That the required artifacts are present and of appropriate quality, _or_
- That we are intentionally deviating from our process, and why.
