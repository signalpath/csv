# SignalPath Computer Systems Validation

## Architecture

### Overview
The purpose of the **Architecture** phase is to identify a potential technical direction for the project as well as any technical risks which the development team may face.

### Entry Criteria

#### Required Artifacts

- From the **[Business Requirements](business_requirements.md)** phase
    - a Jira issue of type Major Feature with at least one of the following
        - Use Case Diagrams
        - Wireframes
        - High-fidelity UI Mockups
- The following staff identified:
    - The Architecture Team (identified by the CTO)

#### Optional Artifacts
- From the **[Business Requirements](business_requirements.md)** phase:
    - Design Documents
    - Activity Diagrams

### Exit Criteria

#### Required Artifacts
Not every project or initiative requires changes to our architecture. Therefore, this phase has no required artifacts.

#### Optional Artifacts
When appropriate, any of the following items may be included in the architecture bundle.

- A technology stack diagram that depicts the major software, service and hardware components that need to be introduced, removed and/or modified.
- A list of change cases, which identify potential architecture-level requirements that our system may need to support. Change cases allow us to discuss the long-term viability of our architecture without requiring us to overbuild our system. We can think through the impact of likely changes to build confidence that the system will still work in the future.
- One or more Technical Enablement issues in Jira. These issues should capture technical (e.g. infrastructure, deployment, integration, etc.) work that needs to be done during the project.
- Sequence diagrams

#### Sign Off
The CTO signs off on the **Architecture** for a major feature by:

- If appropriate, ensuring that Technical Enablement issues are written and linked back to the Major Feature issue.
- Advancing each Technical Enablement issue to Grooming.
- If appropriate, linking change cases and diagrams to the relevant Technical Enablement issues or the Major Feature issue.

The CTO must specify one of the following when signing off:

- That no architecture artifacts were/are necessary for this project, _or_
- That the appropriate artifacts were produced.

## References
Much of this was inspired by http://agilemodeling.com/essays/initialArchitectureModeling.htm
