# SignalPath Computer Systems Validation

## Stories

### Overview
The purpose of the **Stories** phase is to solve or prevent the communication problem that can exist between those who want new functionality  and those who will build the new functionality.

### Entry Criteria

#### Required Artifacts
- The following staff identified:
    - The Product Owner (identified by Product Management)

#### Optional Artifacts
- From the **[Business Requirements](business_requirements.md)** phase
    - a Jira issue of type Major Feature with at least one of the following
        - Use Case Diagrams
        - Wireframes
        - High-fidelity UI Mockups
        - Design Documents
        - Activity Diagrams

### Exit Criteria

#### Required Artifacts

At the end of this phase, we need:

- Story is in a state that it is ready to be groomed
- The following staff members identified:
    - The Development Team (identified by the CTO)

#### Sign Off
The Product Owner signs off on the **Stories** for a major feature by:

- Advancing stories to Grooming
