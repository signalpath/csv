# SignalPath Computer Systems Validation

## Bug Submittal

### Overview
The purpose of the **Bug Submittal** phase is to ensure that a known defect is properly documented so that it can be fixed.

### Entry Criteria

#### Required Artifacts
None

### Exit Criteria

#### Required Artifacts
- A bug report, filed in Jira, that describes:
    - A summary of the bug;
    - Steps to reproduce the bug;
    - The expected result of those steps;
    - The actual result of those steps;
    - Which layer or layers (e.g. platform, UI) the bug exists in;
    - The severity of the bug;
    - The primary cause of the bug, if it is known.

#### Sign Off
The Lead QA Engineer or PO signs off on a bug report by transitioning it to "Grooming".
