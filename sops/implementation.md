# SignalPath Computer Systems Validation

## Implementation

### Overview
The purpose of the **Implementation** phase is to incrementally deliver working software that meets the specified business requirements and architecture.

### Entry Criteria

#### Required Artifacts

- From the **[Business Requirements](business_requirements.md)** phase:
    - Enough good user stories to cover the most important functionality
    - The following staff members identified:
        - The Development Team

_Or_

- From the **[Bug Report](bugs.md)** phase:
    - One or more bugs to be fixed

_And always_

- A story is ready to be implemented when it has been added to a sprint by the Product Owner and the sprint is active.

#### Optional Artifacts

- From the **[Business Requirements](business_requirements.md)** phase:
    - Epics
- From the **[Architecture](architecture.md)** phase:
    - Technology Stack diagram
    - Change cases
    - One or more Technical Enablement issues
    - Sequence diagrams

### Exit Criteria

#### Required Artifacts
The only required artifact is a build artifact of working software that meets our doneness criteria. The minimum criteria for judging doneness is:

- Code Completeness
    - **Peer code reviews** are done, and feedback is dealt with intelligently.
    - Source code is **checked in** to the code library with appropriate comments added. Commit comments reference the appropriate Jira issues.
    - **Unit test cases** have been written and are working successfully.
    - The source code changes have been merged into the appropriate branch in source control. A merge requires 2 reviewers, who are not authors of a commit in the merge, to approve the merge. By approving the merge they agree that the code is in accordance with the project's code review guidelines.
- Testing
    - **Functional testing** is complete.
        - Automated testing. All types of automated test cases have been executed and a test report has been generated. All incidents/defects are reported.
        - Manual testing. The Quality Assurance team has reviewed the reports generated from automation testing and conducted necessary manual test cases to ensure that tests are passing. All incidents/defects are reported.
        - Build issues. If any integration or build issues are found, the necessary steps are repeated and respective "Done" points are adhered to.
    - The appropriate amount of risk-based **regression testing** is done to ensure that defects have not been introduced in the unchanged area of the software.
    - No Severity 1, 2 or 3 bugs are introduced and left unresolved unless approved for deployment by the COO and CTO.
    - **User acceptance testing** is complete. Specifically, for a story, all acceptance criteria are met or for a bug, all issues reported in the bug report have been resolved.

#### Optional Artifacts
When appropriate, the following artifacts can be delivered:

- **Custom deployment notes** for those times when there are special steps that need to be followed during deployment to production.
- **Updated backlog entries** capturing:
    - Any **technical debt** that has been discovered or introduced.
    - If any user story underwent modification or re-scoping during development, the **backlog is updated** to reflect work that still needs to be done.

#### Sign Off

- The QA Engineer signs off on the completion of functional testing and regression testing by advancing the user story or bug to Ready for Acceptance.
- The PO signs off on the completion of user acceptance testing by advancing the user story or bug to Accepted.

## References
Inspired by material from https://www.scrumalliance.org/community/articles/2008/september/definition-of-done-a-reference
