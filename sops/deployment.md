# SignalPath Computer Systems Validation

## Deployment

### Overview

### Entry Criteria

#### Required Artifacts

- From the **[Implementation](implementation.md)** phase:
    - One or more build artifacts for working software
- Approval from the **CTO** to deploy.

#### Optional Artifacts

- From the **[Implementation](implementation.md)** phase:
    - Any relevant deployment notes

### Exit Criteria

#### Required Artifacts

- An updated list of the version(s) of software now running in production.
- Verification that a smoke test passed.

#### Optional Artifacts

- A list of any known Sev 1, 2, 3, defects that would be introduced by deploy that the COO and CTO have approved for deployment and their signoff.

#### Sign Off

- The Lead Engineer, DevOps signs off on the completed deployment by advancing the status of all deployed issues to Shipped.
